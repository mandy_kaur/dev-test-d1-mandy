public class ContactTriggerHandler {  
    public static void createNewCaseAndTask(Contact[] conList) {
    	String errMessage = '';	
        try {
            Case[] caseList = new List <Case>();
            Task[] taskList = new List<Task>();
            Id[] caseIdList = new List<Id>();
            for(Contact cn: conList) {
                Case c = new Case(Status = 'Working' , Origin = 'New Contact' , OwnerId = cn.OwnerId , AccountId = cn.AccountId , ContactId = cn.Id);
                c.Priority = (cn.Level__c == 'Primary')? 'High' : ((cn.Level__c == 'Secondary')? 'Medium' : ((cn.Level__c == 'Tertiary')? 'Low' : 'Low'));
                caseList.add(c);
             }
             insert caseList;
             for(Case c: caseList) {
                 caseIdList.add(c.Id);
             } 
             Case[] cList = [Select caseNumber, ContactId, Priority from Case where Id IN :caseIdList]; 
             Map<Id, Case> caseMap = new Map<Id, Case>();
             for(Case c: cList) { 
                 caseMap.put(c.ContactId, c);
             }   
             for(Contact cn: conList) {
                 Case cs = caseMap.get(cn.Id);
                 Task t = new Task(WhoId = cn.Id , Priority = 'Normal' );
                 t.Subject = 'Welcome call for ' + ((cn.Salutation == Null)? '':cn.Salutation + ((cn.FirstName == Null)? '':cn.FirstName) + ((cn.LastName == Null)? '':cn.LastName)) + ' - ' + cs.CaseNumber;
                 t.ActivityDate = (cs.Priority == 'High')? date.today().addDays(6) : ((cs.Priority == 'Medium')? date.today().addDays(13) : ((cs.Priority == 'Low')? date.today().addDays(20) : date.today().addDays(20)));                   
                 taskList.add(t);
            }
            insert taskList;
        } catch(DmlException e) {
        	errMessage = e.getMessage();
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
}
