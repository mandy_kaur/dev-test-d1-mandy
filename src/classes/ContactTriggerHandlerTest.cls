@isTest  
public class ContactTriggerHandlerTest {
    static testMethod void checkCaseTaskCreation() {
        List<Contact> cnList = new List<Contact>();
        Integer contactCount = 5000; 
        for ( Integer i=1; i<=contactCount; i++) {
            Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact' + i );
            cnList.add(c);
        }
        test.startTest();
			insert cnList;
	    test.stopTest();
        System.assertEquals(contactCount, [SELECT Count() FROM Case]);
        System.assertEquals(contactCount, [SELECT Count() FROM Task]);       
    }
    static testMethod void checkCaseContactOwner() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Contact cn = [SELECT OwnerId FROM Contact];
        Case cs = [SELECT OwnerId FROM Case];
        System.assertEquals(cn.OwnerId , cs.OwnerId);       
    }
    static testMethod void checkCaseContactId() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Case cs = [SELECT ContactId FROM Case];
        System.assertEquals(c.Id , cs.ContactId);       
    }
    static testMethod void checkCaseContactAccountId() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Contact cn = [SELECT AccountId FROM Contact];
        Case cs = [SELECT AccountId FROM Case];
        System.assertEquals(cn.AccountId , cs.AccountId);       
    }
    static testMethod void checkCaseStatus() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Case cs = [SELECT Status FROM Case];
        System.assertEquals('Working' , cs.Status);       
    }
    static testMethod void checkCaseOrigin() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Case cs = [SELECT Origin FROM Case];
        System.assertEquals('New Contact' , cs.Origin);
    }
    static testMethod void checkCasePriority() {
        Contact[] conList = new List<Contact>();
        Contact c1 = new Contact ( FirstName = 'Test', LastName = 'Contact1', Level__c = 'Primary');
        Contact c2 = new Contact ( FirstName = 'Test', LastName = 'Contact2', Level__c = 'Secondary');
        Contact c3 = new Contact ( FirstName = 'Test', LastName = 'Contact3', Level__c = 'Tertiary');
        conList.add(c1);
        conList.add(c2);
        conList.add(c3);
        insert conList;
        Case[] caseList = [SELECT ContactId, Priority FROM Case];
        Map<Id, Case> caseMap = new Map<Id, Case>();
            for(Case cs: caseList) { 
                 caseMap.put(cs.ContactId, cs);
            }
            Case csRet1 = caseMap.get(c1.Id);
            System.assertEquals('Primary' , c1.Level__c);
            System.assertEquals('High' , csRet1.Priority);
            Case csRet2 = caseMap.get(c2.Id);
            System.assertEquals('Secondary' , c2.Level__c);
            System.assertEquals('Medium' , csRet2.Priority);
            Case csRet3 = caseMap.get(c3.Id);
            System.assertEquals('Tertiary' , c3.Level__c);
            System.assertEquals('Low' , csRet3.Priority);
    }
    static testMethod void checkTaskContactOwner() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Contact cn = [SELECT Id FROM Contact];
        Task t = [SELECT WhoId FROM Task];
        System.assertEquals(cn.Id , t.WhoId);       
    }
    static testMethod void checkTaskPriority() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Task t = [SELECT Priority FROM Task];
        System.assertEquals('Normal' , t.Priority);       
    }
    static testMethod void checkTaskSubject() {
        Contact c = new Contact ( FirstName = 'Test', LastName = 'Contact');
        insert c;
        Contact cn = [SELECT Salutation, FirstName, LastName  FROM Contact];
        Case cs = [SELECT CaseNumber from Case];
        Task t = [SELECT Subject FROM Task];
        System.assertEquals('Welcome call for '+ ((cn.Salutation == Null)? '':cn.Salutation + ((cn.FirstName == Null)? '':cn.FirstName) + ((cn.LastName == Null)? '':cn.LastName))  + ' - ' + cs.CaseNumber , t.Subject);       
    }
    static testMethod void checkTaskActivityDate() {
        Contact[] conList = new List<Contact>();
        conList.add(new Contact ( FirstName = 'Test', LastName = 'Contact1', Level__c = 'Primary'));
        conList.add(new Contact ( FirstName = 'Test', LastName = 'Contact2', Level__c = 'Secondary'));
        conList.add(new Contact ( FirstName = 'Test', LastName = 'Contact3', Level__c = 'Tertiary'));
        insert conList;
        
        Case[] caseList = [SELECT ContactId, Priority FROM Case];
        Task[] taskList = [SELECT WhoId, ActivityDate FROM TasK];
        ContactTriggerHandler.createNewCaseAndTask(new List<Contact>{ new Contact()});
        Map<Id, Task> TaskMap = new Map<Id, Task>();
        for(Task t: taskList) { 
             taskMap.put(t.WhoId, t);
        }
        Task tRet1 = taskMap.get(caseList[0].ContactId);
        System.assertEquals(date.today().addDays(6) , tRet1.ActivityDate);
        System.assertEquals('High' , caseList[0].Priority);
        
        Task tRet2 = taskMap.get(caseList[1].ContactId);
        System.assertEquals(date.today().addDays(13) , tRet2.ActivityDate);
        System.assertEquals('Medium' , caseList[1].Priority);
        
        Task tRet3 = taskMap.get(caseList[2].ContactId);
        System.assertEquals(date.today().addDays(20) , tRet3.ActivityDate);
        System.assertEquals('Low' , caseList[2].Priority);
    }		    
}
